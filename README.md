# Usage #
7000 to 7010 are the ports for http or tcp streams.
```
docker run -p 9393:9393 -p 7000-7010:7000-7010 -v /tmp/spring-flow/logs:/tmp/spring-flow/logs somospnt/spring-cloud-data-flow-docker
```

# Docker compose #

```
git clone https://bitbucket.org/somospnt/spring-cloud-data-flow-docker.git
```

```
docker-compose up -d
```

# Test #

```
http://host:9393/dashboard
```